import XCTest

enum Location {
    case left;
    case right;
    case front;
    case end;
    case current;
}

class Node<T> {
    let data : T?;
    let next : Node<T>?;

    init(copy node : Node<T>?) {
        data = node?.data;
        next = nil;
    }
    
    init(data value : T?, next node : Node<T>?) {
        data = value;
        next = node;
    }
}

struct ZipperIterator<T> : IteratorProtocol {
    
    var current : Node<T>?;
    
    init(start node : Node<T>?) {
        current = node;
    }
    
    mutating func next() -> Node<T>? {
        if let node = current {
            defer { current = node.next };
            return node;
        } else { return nil }
        
    }
}

class ZipperList<T> : Sequence {
    
    private let head   : Node<T>?;
    private let focus  : Node<T>?;
    private let tail   : Node<T>?;
    private let length : Int;
    
    init(head x : Node<T>?, focus node : Node<T>?, tail y : Node<T>?, size length : Int) {
        head  = x;
        focus = node;
        tail  = y;
        self.length = length;
    }
    
    
    
    func move(location dest : Location) -> ZipperList<T> {

        switch dest {

        case .left:
            return ZipperList(head: head?.next, focus: Node(copy: head), tail: Node(data: focus?.data, next: tail), size: length);
        case .right:
            return ZipperList(head: Node(data: focus?.data, next: head), focus: Node(copy: tail), tail: tail?.next, size: length);
        case .front:
            if (head != nil) {
                return self.move(location: .left).move(location: .front);
            } else { return self }
        case .end:
            if (tail != nil) {
                return self.move(location: .right).move(location: .end)
            } else { return self }
        case .current:
            return self;
        }
    }



    func insert(data value : T?, location dest : Location, update move : Bool) -> ZipperList<T> {

        switch dest {

        case .left:
            if (move) {
                return ZipperList(head: Node(data: value, next: head), focus: focus, tail: tail, size: length+1);
            } else {
                return ZipperList(head: head, focus: Node(data: value, next: nil), tail: Node(data: focus?.data, next: tail), size: length+1);
            }
        case .right:
            if (move) {
                return ZipperList(head: head, focus: focus, tail: Node(data: value, next: tail), size: length+1);
            } else {
                return ZipperList(head: Node(data: focus?.data, next: head), focus: Node(data: value, next: nil), tail: tail, size: length+1)
            }
        case .front:
            if (head != nil) {
                return self.move(location: .left).insert(data: value, location: .front, update: move);
            } else {
                return ZipperList(head: head, focus: Node(data: value, next: nil), tail: Node(data: focus?.data, next: tail), size: length+1)
            }
        case .end:
            if (tail != nil) {
                return self.move(location: .right).insert(data: value, location: .end, update: move);
            } else {
                return ZipperList(head: Node(data: focus?.data, next: head), focus: Node(data: value, next: nil), tail: tail, size: length+1)
            }
        case .current:
            return ZipperList(head: head, focus: Node(data: value, next: nil), tail: tail, size: length)

        }
    }

    func delete(location dest : Location) -> ZipperList<T> {

        switch dest {

        case .left:
            return ZipperList(head: head?.next, focus: focus, tail: tail, size: length-1)
        case .right:
            return ZipperList(head: head, focus: focus, tail: tail?.next, size: length-1)
        case .front:
            if (head != nil) {
                return self.move(location: .left).delete(location: .front);
            } else {
                return ZipperList(head: head, focus: Node(copy: tail), tail: tail?.next, size: length-1)
            }
        case .end:
            if (tail != nil) {
                return self.move(location: .right).delete(location: .end)
            } else {
                return ZipperList(head: head?.next, focus: Node(copy: head), tail: tail, size: length-1)
            }
        case .current:
            return ZipperList(head: head, focus: Node(copy: tail), tail: tail?.next, size: length-1)

        }
    }

        func size() -> Int {
            return length;
        }

        func peek(location dest : Location) -> Node<T>? {

            switch dest {

            case .left:
                return head;
            case .right:
                return tail;
            case .current:
                return focus
            case .front:
                if (head != nil) {
                    let temp = self.move(location: .front);
                    return temp.focus;
                } else { return focus}
            case .end:
                if (tail != nil) {
                    let temp = self.move(location: .end);
                    return temp.focus;
                } else { return focus }

        }
    }
    
    func makeIterator() -> ZipperIterator<T> {
        let temp = self.move(location: .front);
        return ZipperIterator(start: Node(data: temp.focus?.data, next: temp.tail));
    }
}

class NodeTest : XCTestCase {
    
    var node : Node<Int>!;
    
    override func setUp() {
        super.setUp();
        node = Node(data: 14, next: Node(data: 44, next: nil))
    }
    
    func testSetValue() {
        XCTAssertTrue(node.data == 14)
    }
    
    func testNextNode() {
        let next : Node<Int> = Node(data: 44, next: nil);
        node = Node(data: 14, next: next);
        
        XCTAssertTrue(node.next === next);
    }
}

class ZipperListTest : XCTestCase {
    
    var list : ZipperList<Int>!;

}


NodeTest.defaultTestSuite.run();
ZipperListTest.defaultTestSuite.run();
